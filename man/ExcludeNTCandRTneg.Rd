% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Dirk_heatmap_Functions.R
\name{ExcludeNTCandRTneg}
\alias{ExcludeNTCandRTneg}
\title{ExcludeNTCandRTneg}
\usage{
ExcludeNTCandRTneg(rain.wide, GLOBAL.allnumericStart = 1)
}
\arguments{
\item{rain.wide}{data frame in the casted form}

\item{GLOBAL.allnumericStart}{start of the numeric data columns for rain.wide}
}
\value{
rain.wide
}
\description{
excluding vironomic core added NTC from further consideration
}
